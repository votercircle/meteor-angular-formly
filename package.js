Package.describe({
  name: 'votercircle:meteor-angular-formly',
  version: '1.1.1',
  summary: 'The awesome angular formly packaged for another superstar!',
  git: 'https://bitbucket.org/votercircle/meteor-angular-formly',
  documentation: 'README.md'
});

Package.onUse(function(api) {
    // meteor version
    api.versionsFrom('1.0');

    api.use('urigo:angular@0.8.0');

    api.addFiles('api-check.js', 'client');
    api.addFiles('angular-aria.js', 'client');
    api.addFiles('angular-formly.js', 'client');
    api.addFiles(['formlyLumx.js', 'formlyLumx.css'], 'client');
});